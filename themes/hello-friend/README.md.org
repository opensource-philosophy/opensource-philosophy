* Hello Friend
  :PROPERTIES:
  :CUSTOM_ID: hello-friend
  :END:
#+caption: Hello Friend
[[https://github.com/panr/hugo-theme-hello-friend/blob/master/images/screenshot.png?raw=true]]

*** DEMO - https://hugo-hello-friend.now.sh/
    :PROPERTIES:
    :CUSTOM_ID: demo---httpshugo-hello-friend.now.sh
    :END:

--------------

- [[#hello-friend][Hello Friend]]
  - [[#demo][DEMO - https://hugo-hello-friend.now.sh/]]
  - [[#features][Features]]
    - [[#built-in-shortcodes][Built-in shortcodes]]
    - [[#code-highlighting][Code highlighting]]
    - [[#improved-rss-feed][Improved RSS Feed]]
  - [[#how-to-start][How to start]]
  - [[#how-to-run-your-site][How to run your site]]
  - [[#how-to-configure][How to configure]]
  - [[#how-to-add-a-cover-image-to-your-posts][How to add a cover image
    to your posts]]
  - [[#how-to-display-the-last-modified-date-in-your-posts][How to
    display the Last Modified Date in your posts]]
  - [[#how-to-hide-read-more-button][How to hide "Read more" button]]
  - [[#add-ons][Add-ons]]
  - [[#how-to-edit][How to (safely) edit the theme]]
  - [[#bug][Found a bug?]]
  - [[#feature][New cool idea or feature]]
  - [[#hello-friend-theme-user][=Hello Friend= theme user?]]
  - [[#sponsoring][Sponsoring]]
  - [[#license][License]]

** Features
   :PROPERTIES:
   :CUSTOM_ID: features
   :END:
- *dark/light mode*, depending on your preferences (dark is default, but
  you can change it)
- great reading experience thanks to [[https://rsms.me/inter/][*Inter
  font*]], made by [[https://rsms.me/about/][Rasmus Andersson]]
- nice code highlighting thanks to [[https://prismjs.com][*PrismJS*]]
- fully responsive

**** Built-in shortcodes
     :PROPERTIES:
     :CUSTOM_ID: built-in-shortcodes
     :END:
- *=image=* (prop required: *=src=*; props optional: *=alt=*,
  *=position=* (*left* is default | center | right), *=style=*)
  - eg:
    ={{< image src="/img/hello.png" alt="Hello Friend" position="center" style="border-radius: 8px;" >}}=
- *=figure=* (same as =image=, plus few optional props: *=caption=*,
  *=captionPosition=* (left | *center* is default | right),
  *=captionStyle=*
  - eg:
    ={{< figure src="/img/hello.png" alt="Hello Friend" position="center" style="border-radius: 8px;" caption="Hello Friend!" captionPosition="right" captionStyle="color: red;" >}}=
- *=imgproc=* Hugo shortcode for image processing, plus additional
  *=position=* param [ left | center | right ] (optional).
  - eg: ={{< imgproc "img/hello.png" Resize "250x" center />}}=
  - More detailed info on processing commands at
    [[https://gohugo.io/content-management/image-processing/]]
- *=code=* (prop required: *=language=*; props optional: *=title=*,
  *=id=*, *=expand=* (default "△"), *=collapse=* (default "▽"),
  *=isCollapsed=*)
  - eg:

  #+begin_example
  {{< code language="css" title="Really cool snippet" id="1" expand="Show" collapse="Hide" isCollapsed="true" >}}
  pre {
    background: #1a1a1d;
    padding: 20px;
    border-radius: 8px;
    font-size: 1rem;
    overflow: auto;

    @media (--phone) {
      white-space: pre-wrap;
      word-wrap: break-word;
    }

    code {
      background: none !important;
      color: #ccc;
      padding: 0;
      font-size: inherit;
    }
  }
  {{< /code >}}
  #+end_example

**** Code highlighting
     :PROPERTIES:
     :CUSTOM_ID: code-highlighting
     :END:
By default the theme is using PrismJS to color your code syntax. All you
need to do is to wrap you code like this:

#+begin_html
  <pre>
  ```html
    // your code here
  ```
  </pre>
#+end_html

*Supported languages*: bash/shell, css, clike, javascript, apacheconf,
actionscript, applescript, c, csharp, cpp, coffeescript, ruby, csp,
css-extras, diff, django, docker, elixir, elm, markup-templating,
erlang, fsharp, flow, git, go, graphql, less, handlebars, haskell, http,
java, json, kotlin, latex, markdown, makefile, objectivec, ocaml, perl,
php, php-extras, r, sql, processing, scss, python, jsx, typescript,
toml, reason, textile, rust, sass, stylus, scheme, pug, swift, yaml,
haml, twig, tsx, vim, visual-basic, wasm.

**** Improved RSS Feed
     :PROPERTIES:
     :CUSTOM_ID: improved-rss-feed
     :END:
Some enhancements have been made to Hugo's
[[https://github.com/gohugoio/hugo/blob/25a6b33693992e8c6d9c35bc1e781ce3e2bca4be/tpl/tplimpl/embedded/templates/_default/rss.xml][internal
RSS]] generation code.

*A page's cover image now appears at the top of its feed display*. This
image is set manually using
[[#how-to-add-a-cover-image-to-your-posts][the cover params]]. If unset,
the RSS generator searches for the first image file in the page bundle
whose name includes 'featured', 'cover', or 'thumbnail'.

*You can optionally display the full page content in your RSS feed*
(default is Description or Summary data from Front Matter). Set
=rssFullText = true= in your =config.toml= file to enable this option.

*You can choose a site image to be displayed when searching for your RSS
feed.* Set =rssImage = "image/url/here"= in your =config.toml= file to
enable this option.

** How to start
   :PROPERTIES:
   :CUSTOM_ID: how-to-start
   :END:
You can download the theme manually by going to
[[https://github.com/panr/hugo-theme-hello-friend.git]] and pasting it
to =themes/hello-friend= in your root directory.

You can also clone it directly to your Hugo folder:

#+begin_src sh
git clone https://github.com/panr/hugo-theme-hello-friend.git themes/hello-friend
#+end_src

If you don't want to make any radical changes, it's the best option,
because you can get new updates when they are available. To do so,
include it as a git submodule:

#+begin_src sh
git submodule add -f https://github.com/panr/hugo-theme-hello-friend.git themes/hello-friend
#+end_src

⚠️ *The theme needs at least Hugo version 0.74.x*.

** How to run your site
   :PROPERTIES:
   :CUSTOM_ID: how-to-run-your-site
   :END:
From your Hugo root directory run:

#+begin_example
hugo server -t hello-friend
#+end_example

and go to =localhost:1313= in your browser. From now on all the changes
you make will go live, so you don't need to refresh your browser every
single time.

** How to configure
   :PROPERTIES:
   :CUSTOM_ID: how-to-configure
   :END:
The theme doesn't require any advanced configuration. Just copy:

#+begin_example
baseurl = "/"
languageCode = "en-us"
theme = "hello-friend"
paginate = 5

[params]
  # dir name of your blog content (default is `content/posts`).
  # the list of set content will show up on your index page (baseurl).
  contentTypeName = "posts"

  # "light" or "dark"
  defaultTheme = "dark"

  # if you set this to 0, only submenu trigger will be visible
  showMenuItems = 2

  # Show reading time in minutes for posts
  showReadingTime = false

  # Show table of contents at the top of your posts (defaults to false)
  # Alternatively, add this param to post front matter for specific posts
  # toc = true

  # Show full page content in RSS feed items
  #(default is Description or Summary metadata in the front matter)
  # rssFullText = true

[languages]
  [languages.en]
    title = "Hello Friend"
    subtitle = "A simple theme for Hugo"
    keywords = ""
    copyright = ""
    menuMore = "Show more"
    writtenBy = "Written by"
    readMore = "Read more"
    readOtherPosts = "Read other posts"
    newerPosts = "Newer posts"
    olderPosts = "Older posts"
    minuteReadingTime = "min read"
    dateFormatSingle = "2006-01-02"
    dateFormatList = "2006-01-02"
    # leave empty to disable, enter display text to enable
    # lastModDisplay = ""

    [languages.en.params.logo]
      logoText = "hello friend"
      logoHomeLink = "/"
    # or
    #
    # path = "/img/your-example-logo.svg"
    # alt = "Your example logo alt text"

    [languages.en.menu]
      [[languages.en.menu.main]]
        identifier = "about"
        name = "About"
        url = "/about"
      [[languages.en.menu.main]]
        identifier = "showcase"
        name = "Showcase"
        url = "/showcase"
#+end_example

to =config.toml= file in your Hugo root directory and change params
fields. In case you need, here's
[[https://gist.github.com/panr/8f9b363e358aaa33f6d353c77feee959][a YAML
version]].

*NOTE:* Please keep in mind that currently main menu doesn't support
nesting.

** How to add a cover image to your posts
   :PROPERTIES:
   :CUSTOM_ID: how-to-add-a-cover-image-to-your-posts
   :END:
Adding a cover image to your post is simple and there are two options
when you edit your =index.md= file in
=content/posts/blog-entry-xy/index.md=:

- Use =cover = "/path/to/absolute/img.jpg"= to link an absolute image
  - Resulting in =https://www.yourpage.com/path/to/absolute/img.jpg=
- Use =cover = "img.jpg"= and =useRelativeCover = true= to link the
  image relative to the blog post folder
  - Resulting in =https://www.yourpage.com/posts/blog-entry-xy/img.jpg=
- Use =coverAlt = "description of image"= to add custom alt text to the
  cover image (defaults to post or page title as alt text)
- Use
  =coverCaption = "Image Credit to [Barry Bluejeans](https://unsplash.com/)"=
  to add a caption for the cover image.

** How to display the Last Modified Date in your posts
   :PROPERTIES:
   :CUSTOM_ID: how-to-display-the-last-modified-date-in-your-posts
   :END:
Add =lastModDisplay = "[your display text]"= to =config.toml= to enable
last modified date on your posts. Note - an empty string value =""= does
not display anything.

Example: =lastModDisplay = "Modified:"= --> "Modified: Jan 01, 0001"

:octocat: Hugo's =enableGitInfo= option is a nice complement to this
feature.

** How to hide "Read more" button
   :PROPERTIES:
   :CUSTOM_ID: how-to-hide-read-more-button
   :END:
In a post's front matter you have to add =hideReadMore= param set to
=true=. This will result in that the post won't have "Read more" button
in the list view.

** Add-ons
   :PROPERTIES:
   :CUSTOM_ID: add-ons
   :END:
- *Archive* --- Theme has built-in =archive= page for main content (see
  =contentTypeName= variable in config). If you need archive on your
  blog just copy
  https://github.com/panr/hugo-theme-hello-friend/blob/master/exampleSite/content/archive.md
  to your =content= dir. If you need multilangual archives, duplicate
  =content/archive.md= and add =.Lang= variable, eg:
  =content/archive.pl.md= (remember to change =url= in duplicated file).
- *Comments* --- for adding comments to your blog posts please take a
  look at =layouts/partials/comments.html=
  https://github.com/panr/hugo-theme-terminal/blob/master/layouts/partials/comments.html.
- *Prepended =<head>=* --- if you need to add something inside =<head>=
  element, and before any of the theme's =<script>= and =<link>= tags
  are declared, please take a look at
  =layouts/partial/prepended_head.html=
  https://github.com/panr/hugo-theme-hello-friend/blob/master/layouts/partials/prepended_head.html
- *Extended =<head>=* --- if you need to add something inside =<head>=
  element, after all of all of the theme's =<script>= and =<link>= tags
  are declared, please take a look at
  =layouts/partial/extended_head.html=
  https://github.com/panr/hugo-theme-hello-friend/blob/master/layouts/partials/extended_head.html
- *Extended =<footer>=* --- if you need to add something before end of
  =<body>= element, please take a look at
  =layouts/partial/extended_footer.html=
  https://github.com/panr/hugo-theme-hello-friend/blob/master/layouts/partials/extended_footer.html

** How to (safely) edit the theme
   :PROPERTIES:
   :CUSTOM_ID: how-to-safely-edit-the-theme
   :END:
If you have to override only some of the styles, you can do this easily
by adding =static/style.css= in your root directory and point things you
want to change.

To change something directly in the theme, you have to go to
=themes/hello-friend= and modify the files.

First, you need to install Node dependencies. To do so, go to the theme
directory (from your Hugo root directory):

#+begin_src sh
 cd themes/hello-friend
#+end_src

then run:

#+begin_src sh
npm install
npm i yarn
yarn
#+end_src

After you modified the files you can run webpack in watch mode:

#+begin_src sh
yarn dev
#+end_src

or rebuild theme

#+begin_src sh
yarn build
#+end_src

To see the changes (remember to restart =hugo server=).

** Found a bug?
   :PROPERTIES:
   :CUSTOM_ID: found-a-bug
   :END:
If you spot any bugs, please use
[[https://github.com/panr/hugo-theme-hello-friend/issues][Issue
Tracker]] or create a new
[[https://github.com/panr/hugo-theme-hello-friend/pulls][Pull Request]]
to fix the issue.

** New cool idea or feature?
   :PROPERTIES:
   :CUSTOM_ID: new-cool-idea-or-feature
   :END:
The theme is in constant development since 2019 and has got many cool
features that helped many of you and made the theme better. But there
were also many features that I wasn't sure about because I want to keep
the theme as simple as possible.

So, let's say you have an idea of how to extend the theme. That's cool
and you're welcome to do that, just follow these steps:

- fork the theme
- implement the feature
- write an instruction how to use the feature
- give a working example of the implementation for other users
- add info about your work to =COMMUNITY-FEATURES.md=
- make a PR with edited =COMMUNITY-FEATURES.md=

This will help keeping the theme close to its roots, and also allow
anyone who wishes to improve it and match their needs, to do whatever
they want.

Sounds OK? Cool, let's rock! 🤘

** =Hello Friend= theme user?
   :PROPERTIES:
   :CUSTOM_ID: hello-friend-theme-user
   :END:
I'd be happy to know more about you and what you are doing. If you want
to share it, please make a contribution and
[[https://github.com/panr/hugo-theme-hello-friend/blob/master/USERS.md][add
your site to the list]]! 🤗

** Sponsoring
   :PROPERTIES:
   :CUSTOM_ID: sponsoring
   :END:
If you like my work and want to support the development of the project,
now you can! Just:

** License
   :PROPERTIES:
   :CUSTOM_ID: license
   :END:
Copyright © 2019-2020 Radosław Kozieł
([[https://twitter.com/panr][[cite/t:@panr]]])

The theme is released under the MIT License. Check the
[[https://github.com/panr/hugo-theme-hello-friend/blob/master/LICENSE.md][original
theme license]] for additional licensing information.
