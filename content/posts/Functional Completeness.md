+++
title = "Functional Completeness"
description = "In most definitions of classical logic, some connectives are introduced as abbreviations of other, so-called primitive connectives. But why does it work, and why should one bother instead of just introducing every connective as primitive? In this post, I am going to tackle these questions by explaining what functional completeness is and how it is used."
date = 2023-10-08T00:00:00+02:00
tags = ["logic", "syntax"]
type = "post"
draft = false
tikzjax = false
+++

<div class="ox-hugo-toc toc has-section-numbers">

<div class="heading">Table of Contents</div>

- <span class="section-num">1</span> [The Interdefinability of Connectives](#the-interdefinability-of-connectives)
- <span class="section-num">2</span> [The Interdefinability of Quantifiers](#the-interdefinability-of-quantifiers)
- <span class="section-num">3</span> [A Definition of Functional Completeness](#a-definition-of-functional-completeness)
- <span class="section-num">4</span> [The Sheffer Stroke and Peirce's Arrow](#the-sheffer-stroke-and-peirce-s-arrow)
- <span class="section-num">5</span> [The Use of Functional Completeness](#the-use-of-functional-completeness)
- <span class="section-num">6</span> [Functional vs. Deductive Completeness](#functional-vs-dot-deductive-completeness)
- [Literature](#literature)

</div>
<!--endtoc-->


## <span class="section-num">1</span> The Interdefinability of Connectives {#the-interdefinability-of-connectives}

As a first approximation, let us get familiar with one very simple idea: Some logical constants can be understood as abbreviations of others. For example, you can introduce the material conditional by stipulating that


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_7210548f159c77d6b59715e8cfef6d8f1e3f192c.svg" alt="\begin{equation*}
\tag{1} \hskip -15em
\text{``}\!\mathit{A} \to \mathit{B}\,\text{''}
\text{ abbreviates }
\text{``}\lnot ( \mathit{A} \land \lnot \mathit{B})\text{''}
\end{equation*}
" class="org-svg" />
</span>
</div>

In our example, we used two connectives, <img src="/opensource-philosophy/ltximg/Functional Completeness_86f0ca7a1249d9845b15768feeadc7af649a4196.svg" alt="$\text{``}\lnot \text{''}$" class="org-svg" /> and <img src="/opensource-philosophy/ltximg/Functional Completeness_a2982f521b88d7f0997f5021babb242300dc3268.svg" alt="$\text{``}\land\text{''}$" class="org-svg" />, to define another one --  <img src="/opensource-philosophy/ltximg/Functional Completeness_464d1cc1cc4393e32a7472e850d219dee7842e37.svg" alt="$\text{``}\to\text{''}$" class="org-svg" />. Note that this is a strict _syntactical definition_ in the following sense: Nothing is claimed about the meaning of <img src="/opensource-philosophy/ltximg/Functional Completeness_9c88f89abac0f09f617cfe3a3aa2d5e89e45c9b3.svg" alt="$\text{``}\!\mathit{A} \to \mathit{B}\,\text{''}$" class="org-svg" /> or <img src="/opensource-philosophy/ltximg/Functional Completeness_78218827967fe4960b285c37ac816a56961f669a.svg" alt="$\text{``}\lnot ( \mathit{A} \land \lnot \mathit{B})\text{''}$" class="org-svg" />. Instead, something is claimed about the arrangement of (sequences of) symbols, namely that the sequence of a well-formed-formula followed by an arrow-sign and another well-formed formula in this order may always replace the sequence of a hook-sign, a left parenthesis, a well-formed-formula, a hat-sign, a hook-sign and a well-formed formula in that order.

Nonetheless, we of course have a semantic interpretation in mind: We want the _definiens_ to yield the same truth-values of the ones usually attributed to the _definiendum_. In short: <img src="/opensource-philosophy/ltximg/Functional Completeness_78218827967fe4960b285c37ac816a56961f669a.svg" alt="$\text{``}\lnot ( \mathit{A} \land \lnot \mathit{B})\text{''}$" class="org-svg" /> should have the same truth-conditions as the ones <img src="/opensource-philosophy/ltximg/Functional Completeness_9c88f89abac0f09f617cfe3a3aa2d5e89e45c9b3.svg" alt="$\text{``}\!\mathit{A} \to \mathit{B}\,\text{''}$" class="org-svg" /> would have if it was not introduced as an abbreviation. That this is the case for our definition can easily be shown by constructing a truth-table:

<span class="org-target" id="org-target--1"></span>


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_5b65b5a37187cbc984e3a04f446fe56f879ab2af.svg" alt="\begin{array}{|c|c|c|c|c|c|c|}
  \hline
  \mathit{A}
  &amp;amp;  \mathit{B}
  &amp;amp; \lnot \mathit{A}
  &amp;amp; \lnot \mathit{B}
  &amp;amp;  \mathit{A}  \land \neg  \mathit{B}
  &amp;amp; \lnot (\mathit{A}  \land \neg  \mathit{B})
  &amp;amp;  \mathit{A}  \to  \mathit{B} \\
  \hline
  T &amp;amp; T &amp;amp; F &amp;amp; F &amp;amp; F &amp;amp; T &amp;amp; T\\ \hline
  T &amp;amp; F &amp;amp; F &amp;amp; T &amp;amp; T &amp;amp; F &amp;amp; F\\ \hline
  F &amp;amp; T &amp;amp; T &amp;amp; F &amp;amp; F &amp;amp; T &amp;amp; T\\ \hline
  F &amp;amp; F &amp;amp; T &amp;amp; T &amp;amp; F &amp;amp; T &amp;amp; T\\
  \hline
\end{array}
" class="center" />
</span>
<span class="equation-label">1</span>
</div>

As we can see, the truth-conditions for <img src="/opensource-philosophy/ltximg/Functional Completeness_c167df3552f0fce9008b8b622e905b5a0dbc331e.svg" alt="$\lnot (\mathit{A}  \land \neg  \mathit{B})$" class="org-svg" /> are the same as those <img src="/opensource-philosophy/ltximg/Functional Completeness_de80173382971832da8a3475c8af298c80b18522.svg" alt="$p \to q$" class="org-svg" /> has in a classical system of logic. Thus, we do not need to introduce a new symbol to our alphabet and define its truth-conditions; it suffices to read <img src="/opensource-philosophy/ltximg/Functional Completeness_fcddec0277dbc082db64a56e8e020b417754fce7.svg" alt="$\text{``}\lnot (\mathit{A} \land \lnot \mathit{B})\text{''}$" class="org-svg" /> as "If A, then B".

Following the same procedure, we can also define
 <img src="/opensource-philosophy/ltximg/Functional Completeness_6df57e8ff8e1b8d8e929df5f92ed784968df4abb.svg" alt="$\text{``}\lor\text{''}$" class="org-svg" /> and <img src="/opensource-philosophy/ltximg/Functional Completeness_f3da801c9be5a65078add5b4fb00842bd344405b.svg" alt="$\text{``}\leftrightarrow\text{''}$" class="org-svg" /> in terms of  <img src="/opensource-philosophy/ltximg/Functional Completeness_86f0ca7a1249d9845b15768feeadc7af649a4196.svg" alt="$\text{``}\lnot \text{''}$" class="org-svg" />  and <img src="/opensource-philosophy/ltximg/Functional Completeness_a2982f521b88d7f0997f5021babb242300dc3268.svg" alt="$\text{``}\land\text{''}$" class="org-svg" />:
<span class="org-target" id="org-target--2"></span>


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_638d9fe76c85ab5cc7e1ec29f2e1b554c0568df9.svg" alt="\begin{equation*}
\tag{2} \hskip -15em
\text{``}\!\mathit{A} \lor \mathit{B}\,\text{''}
\text{ abbreviates }
\text{``}\lnot ( \lnot \mathit{A} \land \lnot \mathit{B})\text{''}
\end{equation*}
" class="equation-double" />
</span>
</div>

<span class="org-target" id="org-target--3"></span>


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_913b8ac0ca0633163193ecff332b202a357fbff7.svg" alt="\begin{equation*}
\tag{3} \hskip -15em
\text{``}\!\mathit{A} \leftrightarrow \mathit{B}\,\text{''}
\text{ abbreviates }
\text{``}\lnot (\mathit{A} \land \lnot \mathit{B}) \land \lnot (\mathit{B} \land \lnot \mathit{A})\text{''}
\end{equation*}
" class="org-svg" />
</span>
</div>

With a natural language interpretation, we can interpret [(2)](#org-target--2) as stipulating that "or" abbreviates "not both are false" and [(3)](#org-target--3) as stipulating that "if and only if" abbreviates "not just one is true or false". If we wanted to, we could also define the strong alternation like this:


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_845c4d2b32731d004f817e39e272f8bdf9aec08c.svg" alt="\begin{equation*}
\label{eq:4}
\tag{4} \hskip -15em
\text{``}\!\mathit{A} \nabla \mathit{B}\,\text{''}
\text{ abbreviates }
\text{``} \lnot (\lnot (\mathit{A} \land \lnot \mathit{B}) \land \lnot (\mathit{B} \land \lnot \mathit{A}))\text{''}
\end{equation*}
" class="org-svg" />
</span>
</div>

<span class="org-target" id="org-target--4"></span>
As it happens, we have now used two connectives -- <img src="/opensource-philosophy/ltximg/Functional Completeness_d5d757ef4b9cb6fa93c4714319f7e2854fca73a1.svg" alt="$\text{``}\neg\text{''}$" class="org-svg" /> and <img src="/opensource-philosophy/ltximg/Functional Completeness_0b5787cdc1738fdc92346726acc93d61b56b417b.svg" alt="$\text{``}\kern-.5ex\land\kern-.5ex\text{''}$" class="org-svg" /> -- to show that all other connectives can be introduced as abbreviations of them, in the sense that the definitions yield the same truth-values as the connectives if they were introduced as primitive symbols.

This is not self-evident, since some pairs of connectives are not capable of defining all others; for example <img src="/opensource-philosophy/ltximg/Functional Completeness_0b5787cdc1738fdc92346726acc93d61b56b417b.svg" alt="$\text{``}\kern-.5ex\land\kern-.5ex\text{''}$" class="org-svg" /> and <img src="/opensource-philosophy/ltximg/Functional Completeness_3d1efea345d4020d1293070850aae72175f8c44c.svg" alt="$\text{``}\kern-.5ex\lor\kern-.5ex\text{''}$" class="org-svg" /> cannot because there is no way to get to <img src="/opensource-philosophy/ltximg/Functional Completeness_6c79af52867297230725512cd68d292ecbed4fe2.svg" alt="$\text{``}\lnot A\text{''}$" class="org-svg" />, <img src="/opensource-philosophy/ltximg/Functional Completeness_86f0ca7a1249d9845b15768feeadc7af649a4196.svg" alt="$\text{``}\lnot \text{''}$" class="org-svg" /> or <img src="/opensource-philosophy/ltximg/Functional Completeness_94560432c79619a9d86c582d4e5134090a095cfb.svg" alt="$\text{``}\kern-.5ex\leftrightarrow\kern-.5ex\text{''}$" class="org-svg" />.


## <span class="section-num">2</span> The Interdefinability of Quantifiers {#the-interdefinability-of-quantifiers}

Now we know that under certain conditions, we only need a smaller set of connectives to define the whole set of connectives. While this suffices for propositional logic, there are two more logical symbols[^fn:1] in _predicate_ _logic_ we need to take into consideration: <img src="/opensource-philosophy/ltximg/Functional Completeness_7988cf2c202838ec809477c603d46ad108a9ee52.svg" alt="$\text{``}\forall \text{''}$" class="org-svg" /> and <img src="/opensource-philosophy/ltximg/Functional Completeness_85f079b4f6ded44901fe76cca85fc6334678e3a3.svg" alt="$\text{``}\exists \text{''}$" class="org-svg" />. Here, we can also define one quantifier in terms of the other using <img src="/opensource-philosophy/ltximg/Functional Completeness_86f0ca7a1249d9845b15768feeadc7af649a4196.svg" alt="$\text{``}\lnot \text{''}$" class="org-svg" />. For example, we can stipulate that


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_0d168704e57973c190e9c8c7fcab94b8c219bdb9.svg" alt="\begin{equation*}
\tag{5} \hskip -15em
\text{``}\exists\mathit{x}\,\text{''}
\text{ abbreviates }
\text{``} \lnot \forall\mathit{x}\,\lnot\text{''}
\end{equation*}
" class="org-svg" />
</span>
</div>

and thereby define the existential quantifier by means of <img src="/opensource-philosophy/ltximg/Functional Completeness_88dda080e9c07f8a3ff97c3fa5bc5293cfe7e744.svg" alt="$\text{``}\forall\text{''}$" class="org-svg" /> and <img src="/opensource-philosophy/ltximg/Functional Completeness_d5d757ef4b9cb6fa93c4714319f7e2854fca73a1.svg" alt="$\text{``}\neg\text{''}$" class="org-svg" />. To see this is working as intended, we simply need our definition and the rule of Double Negation (both introduction and eliminiation)[^fn:2]:


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_54136e6e61206cef7e5575bd1fe7ef1a183d6400.svg" alt="\begin{chain}
  \neg \forall \mathit{x} \mathit{A}   &amp;amp;&amp;amp; \kgrün{\neg \forall \mathit{x}\, \lnot }\lnot \mathit{A} \hphantom{\lnot\lnot } &amp;amp; DN \\
  &amp;amp;&amp;amp; \exists \mathit{x} \, \lnot \mathit{A}   &amp;amp; Def_\exists
\end{chain} \vskip 1em
\begin{chain}
  \forall \mathit{x} \mathit{A} \hphantom{\lnot }  &amp;amp;&amp;amp; \lnot \kgrün{\lnot \forall \mathit{x} \, \lnot}\lnot \mathit{A} \hphantom{\lnot} &amp;amp;  DN  \\
  &amp;amp;&amp;amp;  \lnot \exists \mathit{x} \, \lnot \mathit{A}   &amp;amp; Def_\exists
\end{chain} \vskip 1em
\begin{chain}
  \forall \mathit{x}\,\lnot  \mathit{A}   &amp;amp;&amp;amp; \lnot \kgrün{\neg \forall \mathit{x} \,  \lnot } \mathit{A} \hphantom{\lnot\lnot }&amp;amp; DN \\
  &amp;amp;&amp;amp; \lnot \dgelb{\exists \mathit{x}} \mathit{A}   &amp;amp; Def_\exists
\end{chain}
" class="center" />
</span>
</div>

One could, of course, also take <img src="/opensource-philosophy/ltximg/Functional Completeness_ba4b7a732878c7f8cf927f0e51480e1556e46d22.svg" alt="$\text{``}\exists\text{''}$" class="org-svg" /> as primitive and define <img src="/opensource-philosophy/ltximg/Functional Completeness_88dda080e9c07f8a3ff97c3fa5bc5293cfe7e744.svg" alt="$\text{``}\forall\text{''}$" class="org-svg" /> in terms of it. Similarly, you can define a weak modal operator like <img src="/opensource-philosophy/ltximg/Functional Completeness_dc4338919ea9c9182bbb5cc798b7f45bea1d3bed.svg" alt="$\text{``}\Diamond \text{''}$" class="org-svg" /> by the corresponding strong one -- <img src="/opensource-philosophy/ltximg/Functional Completeness_e7d490b478f7b00d5032693898cf1501eeef370f.svg" alt="$\text{``}\Box \text{''}$" class="org-svg" /> in this case -- and vice versa.


## <span class="section-num">3</span> A Definition of Functional Completeness {#a-definition-of-functional-completeness}

In the last two sections, we have shown that we only need _some_ connectives and quantifiers in our syntax to express _all_ of them. We also made plausible that not every set of logical constants is capable of doing this, so we are talking about an interesting property here -- a property usually called _functional completeness_ or _expressive completeness_. Now we have an idea of what it is, so let us consider a formal definition. As it happens, there are several definitions of this concept, but they only deviate in minor points. I suggest the following definition, which is based on <a href="#citeproc_bib_item_1">Yaqub, 2015, p. 110</a>:


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_8953fe2bcdd4cb4f515bfb7933c83fe7a1ddb72c.svg" alt="\begin{Large}
\begin{defin}{Functional Completeness}{}
 A \textit{logical operator} is a connective, quantifier or modal operator.  \vskip .5em
  A set S of logical operators is \textcolor{pink}{\textit{functionally complete}} for a language L if and only if every unary and binary logical operator is expressible in L in terms of S.
\end{defin}
\end{Large}
" class="org-svg" />
</span>
</div>

The basic idea of this definition is that if you have _some_ connectives -- represented by the set <img src="/opensource-philosophy/ltximg/Functional Completeness_7a654b4c429c0c2263b17d8c8d9d17be7ae72cbf.svg" alt="$S$" class="org-svg" />, you have _all_ connectives. With the notion of functional completeness in place, we are now able to express what we discovered in sections [1](#the-interdefinability-of-connectives) and [2](#the-interdefinability-of-quantifiers) very briefly:


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_5ed943b3d0ee6e1c88c2538811020c019fb95dd6.svg" alt="\begin{equation*} \hskip -15em
\tag{6} \text{The set $\{ \lnot, \land \}$ is functionally complete for propositional logic.}
\end{equation*}
" class="equation-double" />
</span>
</div>


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_5de63e67e1836495734b62683040ded41b8c2606.svg" alt="\begin{equation*} \hskip -15em
\tag{7} \text{The set $\{\lnot, \land, \forall\}$ is functionally complete for predicate logic.}
\end{equation*}
" class="org-svg" />
</span>
</div>

Note that these are far from all sets of connectives that are functionally complete for propositional or predicate logic. For example, <img src="/opensource-philosophy/ltximg/Functional Completeness_e4036374fe163785e6146ce8b5a9b0da05266668.svg" alt="$\{ \lnot ,\lor,\exists  \}$" class="org-svg" /> is functionally complete, too, as well as <img src="/opensource-philosophy/ltximg/Functional Completeness_d5dbf14a60f73a1ccbfb695c296f47f06144d555.svg" alt="$\{ \bot,\to\}$" class="org-svg" />.


## <span class="section-num">4</span> The Sheffer Stroke and Peirce's Arrow {#the-sheffer-stroke-and-peirce-s-arrow}

Incidentally, you can even get all connectives defined by a _single_ sign, the Sheffer stroke <img src="/opensource-philosophy/ltximg/Functional Completeness_113aebe5fabdb59c15086a83fb3b934285d63dbf.svg" alt="$\text{``}\kern-.5ex\downarrow\kern-.5ex\text{''}$" class="org-svg" />. Its semantics are defined like this:


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_48b1b5c4e81235a931352e8770928b3f5918e4ec.svg" alt="\begin{equation*} \hskip -15em
\tag{8} \mathit{v}(\!\mathit{A} \downarrow \mathit{B}) = 1 \text{ iff } \mathit{v}(\!\mathit{A})=0 \text{ and } \mathit{v}(\!\mathit{B})=0
\end{equation*}
" class="org-svg" />
</span>
</div>

The Sheffer stroke can be read as "neither A nor B". As a limiting case, then, imagine that A and B are the same proposition. Then we can stipulate:

<span class="org-target" id="org-target--eq9"></span>


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_d2bd8844272773d3401a37664a8237e4c26e978f.svg" alt="\begin{equation*} \hskip -15em
\tag{9} \text{``}\neg\mathit{A}\text{'' abbreviates } \text{``}(\mathit{A}\downarrow\mathit{A})\text{''}
\end{equation*}
" class="org-svg" />
</span>
</div>

This also makes sense from a natural-language standpoint: "neither A nor A" is just a peculiar way of saying "not A". The rest of the connectives is now easily defined on the basis of <img src="/opensource-philosophy/ltximg/Functional Completeness_d5d757ef4b9cb6fa93c4714319f7e2854fca73a1.svg" alt="$\text{``}\neg\text{''}$" class="org-svg" /> and <img src="/opensource-philosophy/ltximg/Functional Completeness_113aebe5fabdb59c15086a83fb3b934285d63dbf.svg" alt="$\text{``}\kern-.5ex\downarrow\kern-.5ex\text{''}$" class="org-svg" />.

Similarly, we can define all connectives as abbreviations of _Peirce's Arrow_, whose truth-conditions are defined this way:


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_d0d6f8f7b816a1d36af6ac6f3d180f94a57014f6.svg" alt="\begin{equation*} \hskip -15em
\tag{10} \mathit{v}(\!\mathit{A} \uparrow \mathit{B}) = 1 \text{ iff } \mathit{v}(\!\mathit{A})=0 \text{ or } \mathit{v}(\!\mathit{B})=0
\end{equation*}
" class="org-svg" />
</span>
</div>

Analagous to [(9)](#org-target--eq9), Peirce's arrow can be read as "not both A and B". This allows for the following definition:


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_9633f1c87c9db6776669ecfd8ff5cbe76e76e40a.svg" alt="\begin{equation*} \hskip -15em
\tag{11} \text{``}\neg\mathit{A}\text{'' abbreviates } \text{``}(\mathit{A}\uparrow\mathit{A})\text{''}
\end{equation*}
" class="org-svg" />
</span>
</div>

"Not both A and A", again, simply means the same as "not A". As a side note for computer scientists: The Sheffer stroke corresponds to the `NAND` operator, Peirce's arrow to the `NOR` operator. Peirce's arrow is sometimes also called "Quine's dagger".


## <span class="section-num">5</span> The Use of Functional Completeness {#the-use-of-functional-completeness}

Now that we know what functional completeness is, the question arises why it would matter to talk about it. After all, no matter whether you introduce all connectives as primitive symbols or some of them as abbreviations, all formulas have the same truth-values and proof theory stays the same anyway.

What seems to be a reason against introducing small functionally complete sets of operators in the first place turns out to be just the reason for doing so. On the one hand, thinking about it makes clear which relations the connectives hold to each other, which is a value in itself. It also helps compare different logics. For example, in intuitionistic logic, the quantifiers are not interdefinable, and in three-valued logic, the sets of functionally complete connectives are much more limited.

On the other hand, and most importantly, if you intend to prove something not in, but _about_ a logical language, you will most often find yourself in a situation which requires you to use proof by induction on the complexity of formulae[^fn:3]. This, in effect, means, that you need to show _for all possible combinations of well-formed formulae_ that the proposition you claim holds. By making use of functionally completion, you can reduce the number of those possible combinations by introducing less primitive signs, which makes the proofs much easier. Logicians are humans, after all, and humans are lazy.


## <span class="section-num">6</span> Functional vs. Deductive Completeness {#functional-vs-dot-deductive-completeness}

As a last point, it is important to keep two notions of completeness apart: Functional completeness of a set of connectives is not the same as completeness of a proof system (that is, for example, a set of axioms plus a set of deduction rules); although both establish a connection between a syntactic and a semantic concept, there are some important differences.

Whilst functional completeness is about the connection between abbreviations and functions which map to truth values, completeness of a proof system is concerned with the relationship between the set of universally valid formulae and the set of formulae which result from rule-governed sign manipulation.

| Property of  ...                      | syntactic concept           | semantic concept |
|---------------------------------------|-----------------------------|------------------|
| a set of connectives                  | abbreviations (definitions) | truth-functions  |
| the set of universally valid formulae | deduction                   | validity         |

To get a better idea of how these concepts deviate, consider a language with the same syntax as classical propositional logic and an axiom system of propositional logic, but with the following semantics, which is admittedly not all too inventive:

<span class="org-target" id="org-target--Triv"></span>


<div class="equation-container">
<span class="equation">
<img src="/opensource-philosophy/ltximg/Functional Completeness_e8d74bc3fab1f3d9d9818c2675fe2525cd72d614.svg" alt="\begin{equation*} \hskip -15em
\tag{Triv}
\mathit{v\,(A)}=T
\end{equation*}
" class="img-newline" />
</span>
</div>

With [(Triv)](#org-target--Triv), we state that _any_ formula is true. As a direct corollary, then, any formula is universally valid as well, so also every formula whose main connective is one of  <img src="/opensource-philosophy/ltximg/Functional Completeness_0b5787cdc1738fdc92346726acc93d61b56b417b.svg" alt="$\text{``}\kern-.5ex\land\kern-.5ex\text{''}$" class="org-svg" />, <img src="/opensource-philosophy/ltximg/Functional Completeness_3d1efea345d4020d1293070850aae72175f8c44c.svg" alt="$\text{``}\kern-.5ex\lor\kern-.5ex\text{''}$" class="org-svg" />, <img src="/opensource-philosophy/ltximg/Functional Completeness_07c32701d95389e5c3485280e7b36064840572f4.svg" alt="$\text{``}\kern-.5ex\to\kern-.5ex\text{''}$" class="org-svg" /> <img src="/opensource-philosophy/ltximg/Functional Completeness_86f0ca7a1249d9845b15768feeadc7af649a4196.svg" alt="$\text{``}\lnot \text{''}$" class="org-svg" /> or <img src="/opensource-philosophy/ltximg/Functional Completeness_94560432c79619a9d86c582d4e5134090a095cfb.svg" alt="$\text{``}\kern-.5ex\leftrightarrow\kern-.5ex\text{''}$" class="org-svg" /> is. But since we constructed our system to have a classical set of deducible formulae, some valid formulae are not provable. In other words: Our proof system ist not complete with respect to the semantics we stated.

Nonetheless, any set of connectives is functionally complete in our system: No matter what formula we look at, be it of the form  <img src="/opensource-philosophy/ltximg/Functional Completeness_9c88f89abac0f09f617cfe3a3aa2d5e89e45c9b3.svg" alt="$\text{``}\!\mathit{A} \to \mathit{B}\,\text{''}$" class="org-svg" />, <img src="/opensource-philosophy/ltximg/Functional Completeness_6e7cbe675acd92343283ddd3ec31222914827bce.svg" alt="$\text{``}\neg \mathit{A}\text{''} $" class="org-svg" /> or any other form, it is true. So any abbreviation of one connective by the other results in the same truth-values -- or, as in this case, in the same truth-value.

Note that if we had chosen a proof system in which any formula is deducible, our proof system would be complete with respect to the given semantics. The functional completeness of our system would not have changed, though. This is another important difference between functional completeness and completeness of a proof system: While the former is dependent on a system of deduction, the latter is not.

Was this article of any help to you? If so, consider leaving a comment or supporting me by [buying me a coffee](https://www.buymeacoffee.com/vitus)!


## Literature {#literature}

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Yaqub, A. M. (2015). <i>An Introduction to Metalogic</i>. Broadview Press.</div>
</div>

[^fn:1]: To be exact, there are three logical symbols in predicate logic. Next to the quantifiers, there also is the identity sign <img src="/opensource-philosophy/ltximg/Functional Completeness_4ec992a8bec7839b62859429adfdecd54b0a0e87.svg" alt="$\text{``}=\text{''}$" class="org-svg" />. Since, as will become clear, functional completeness is about _connectives_, and the identity sign is a predicate symbol, it is not relevant to what we are up to. Nonetheless, it is interesting to note that identity cannot be defined in classical first-order logic, but _has_ to be added as a primitive symbol. In mereology, set theory and second-order logic, identity can be defined, though.
[^fn:2]: In intuitionistic logic, the quantifiers are not interdefinable. This is a direct consequence of the fact that intuitionists do not accept DN.
[^fn:3]: A nerdy side note: Proof by induction on the complexity of formulae or length of proofs can be reduced to the principle of mathematical induction, which itself is a theorem of set theory. A very accessible introduction to the PMI can be found in <a href="#citeproc_bib_item_1">Yaqub, 2015, pp. 90–94</a>.