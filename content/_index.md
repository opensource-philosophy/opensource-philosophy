+++
title = "Home"
draft = false
+++

Hello, and welcome to my website [opensource-philosophy](https://opensource-philosophy.com/)! I am Vitus Schäfftlein, top-grade B.A. in Communication Sciences and Philosophy, emacs-enthusiast and self-taught programmer. My interest in both logic and statistics has led me to programming and writing code, where I have been involved in the organization, implementation and maintenance of automization scripts for the [digitalization of Heinrich Scholz's works](https://www.ulb.uni-muenster.de/sammlungen/nachlaesse/nachlass-scholz.html). There, I discovered that I do not only like _acquiring_ analytics skills on a very general level, but that I also love _applying those skills_ to real-world problems. At the moment, I am looking for employment.

If you are interested in my background, you are invited to continue reading [here](/about_me). If you would like to know more about what I do or how I set this blog up, you might also want to have a look at my [repositories on Codeberg](https://codeberg.org/opensource-philosophy/). There are also some [posts](/posts/) you might want to check out. Should you be interested in contacting me, I would be happy to hear from you [via mail](mailto:vitus.schaefftlein@opensource-philosophy.com)!
