+++
title = "My Work at University"
description = "Interested in what I did at university? Here, you can find my top-grade assignments, bachelor thesis and teaching material."
draft = false
+++

## <span class="section-num">1</span> Publications {#publications}

-   Schäfftlein, V., Dietz, K., Dinger, P., Horstmann, J., Normann, I. 2024. Von Menschen und Maschinen: Transdisziplinäre Workflows im Münsteraner Editionsprojekt Heinrich Scholz. [Paper submitted and accepted for the yearly [DHd](https://dig-hum.de/)-Conference]


## <span class="section-num">2</span> Assignments {#assignments}

-   [Functions as Graphs, or Functions as Rules? (Bachelor's Thesis)](/downloads/my_writings/assignments/bachelor_thesis.pdf)
-   [Non seulement Parler, mais aussi agir?](/downloads/my_writings/assignments/parler.pdf)
-   [Und Herrschen ist doch Areté?](/downloads/my_writings/assignments/ist_herrschen_arete.pdf)


## <span class="section-num">3</span> Presentations {#presentations}

-   [Bachelor Thesis Slides (German)](/downloads/my_writings/presentations/bachelor_thesis_slides.pdf)
-   [Deduction Theorem Screencast (German)](/downloads/my_writings/presentations/deduction_theorem_screencast.mp4)
-   [Logical Intuitions Slides (English)](/downloads/my_writings/presentations/logical_intuitions_english_slides.pdf)
-   [Logical Intuitions Sreencast (German)](/downloads/my_writings/presentations/logical_intuitions_german_screencast.mp4)
-   [Logical Intuitions Slides (German)](/downloads/my_writings/presentations/logical_intuitions_german_slides.pdf)
-   [A Note on Truth (German)](/downloads/my_writings/presentations/note_on_truth.pdf)
-   [Tableaux Screencast (German)](/downloads/my_writings/presentations/tableaux_screencast.mp4)


## <span class="section-num">4</span> Teaching {#teaching}


### <span class="section-num">4.1</span> Logic {#logic}

-   [[01] Definitionen](/downloads/my_writings/teaching/logic/01_definitionen.pdf)
-   [[02] Syntax AL](/downloads/my_writings/teaching/logic/02_tafelschwammtest_syntax_al.pdf)
-   [[03] Semantik AL](/downloads/my_writings/teaching/logic/03_mengen_semantik_al.pdf)
-   [[04] Deutung AL](/downloads/my_writings/teaching/logic/04_deutung_al.pdf)
-   [[05] Tableaux AL](/downloads/my_writings/teaching/logic/05_tableaux_al.pdf)
-   [[06] Natürliches Schließen AL](/downloads/my_writings/teaching/logic/06_natürliches_schließen.pdf)
-   [[07] Argumentrekonstruktion Platon](/downloads/my_writings/teaching/logic/07_rekonstruktion_platon.pdf)
-   [[08] Syntax PL](/downloads/my_writings/teaching/logic/08_syntax_pl.pdf)
-   [[09] Semantik PL](/downloads/my_writings/teaching/logic/09_semantik_pl.pdf)
-   [[10] Tableaux PL](/downloads/my_writings/teaching/logic/10_tableaux_pl.pdf)
-   [[11] Identität und Kennzeichnungen](/downloads/my_writings/teaching/logic/11_identit%C3%A4t_kennzeichnungen.pdf)
-   [[12] Klausuvorbereitung](/downloads/my_writings/teaching/logic/12_klausurvorbereitung.pdf)


### <span class="section-num">4.2</span> Data Acquisition {#data-acquisition}

-   [[01] Forschungsprozess](/downloads/my_writings/teaching/data_acquisition/01_forschungsprozess.pdf)
-   [[02] Wissenschaftstheorie](/downloads/my_writings/teaching/data_acquisition/02_wissenschaftstheorie.pdf)
-   [[03] Operationalisierung I](/downloads/my_writings/teaching/data_acquisition/03_operationalisierung_i.pdf)
-   [[04] Operationalisierung II](/downloads/my_writings/teaching/data_acquisition/04_operationalisierung_ii.pdf)
-   [[05] Befragung I](/downloads/my_writings/teaching/data_acquisition/05_befragung_i.pdf)
-   [[06] Befragung II](/downloads/my_writings/teaching/data_acquisition/06_befragung_ii.pdf)
-   [[07] Inhaltsanalyse I](/downloads/my_writings/teaching/data_acquisition/07_inhaltsanalyse_i.pdf)
-   [[08] Inhaltsanalyse II](/downloads/my_writings/teaching/data_acquisition/08_inhaltsanalyse_ii.pdf)
-   [[09] Grundgesamtheit und Stichprobe](/downloads/my_writings/teaching/data_acquisition/09_grundgesamtheit_stichprobe.pdf)
-   [[10] Beobachtung und Experiment](/downloads/my_writings/teaching/data_acquisition/10_beobachtung_experiment.pdf)
-   [[11] Wiederholung](/downloads/my_writings/teaching/data_acquisition/11_wiederholung.pdf)


### <span class="section-num">4.3</span> Statistical Data Analysis {#statistical-data-analysis}

-   [[01] Einführung](/downloads/my_writings/teaching/statistical_data_analysis/01_einführung.pdf)
-   [[02] Dateneingabe](/downloads/my_writings/teaching/statistical_data_analysis/02_dateneingabe.pdf)
-   [[03] Datenmanagement](/downloads/my_writings/teaching/statistical_data_analysis/03_datenmanagement.pdf)
-   [[04] Faktorbeschreibung](/downloads/my_writings/teaching/statistical_data_analysis/04_beschreibung_faktoren.pdf)
-   [[05] Vektorbeschreibung](/downloads/my_writings/teaching/statistical_data_analysis/05_beschreibung_vektoren.pdf)
-   [[06] Schätzen und Testen](/downloads/my_writings/teaching/statistical_data_analysis/06_schätzen_testen.pdf)
-   [[07] Kreuztabellen](/downloads/my_writings/teaching/statistical_data_analysis/07_kreuztabellen.pdf)
-   [[08] Mittelwertunterschiede](/downloads/my_writings/teaching/statistical_data_analysis/08_mittelwertunterschiede.pdf)
-   [[09] Korrelationen](/downloads/my_writings/teaching/statistical_data_analysis/09_korrelationen.pdf)
-   [[10] Regression](/downloads/my_writings/teaching/statistical_data_analysis/10_regression.pdf)
-   [[11] Klausurvorbereitung](/downloads/my_writings/teaching/statistical_data_analysis/11_klausurvorbereitung.pdf)


### <span class="section-num">4.4</span> Miscellaneous {#miscellaneous}

-   [Die Kunst des Quantorenverschiebens](/downloads/my_writings/teaching/die_kunst_des_quantorenverschiebens.pdf)
-   [Drehkreuze](/downloads/my_writings/teaching/drehkreuze.pdf)
-   [Graphen](/downloads/my_writings/teaching/graphen.pdf)
-   [Logic Vocabulary](/downloads/my_writings/teaching/logic_vocabulary.pdf)
