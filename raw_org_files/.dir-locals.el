((org-mode . (; things to do when opening a .org file in folder
; (org-hugo-preserve-filling . nil) ; had problems with export blocks
(org-hugo-use-code-for-kbd . t) ; "~foo~" exports key bindings
(org-hugo-export-with-toc . nil); no TOC by ox-hugo
(org-export-with-toc . t) ; but TOC by org
(org-export-with-title . t) ; export #+title: as post title
(org-export-with-author . nil) ; do not export author name
(org-hugo-export-with-section-numbers . t) ; number post headings
(org-export-default-language . "en"); publish in English
(org-preview-latex-image-directory . "../static/ltximg/") ; svg previews saved in blog directory
(org-blackfriday--ltximg-directory . "opensource-philosophy/ltximg/")
(bibtex-completion-bibliography . ("../static/blog.bib")) ; use thie bibtex-file for citing
(org-export-before-parsing-hook . (lambda (_) (org-ref-process-buffer 'html))) ; needed for org-ref to work
(eval . (org-hugo-auto-export-mode)) ; create md on saving .org file
)))
